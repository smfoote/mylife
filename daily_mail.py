from google.appengine.api import mail
from google.appengine.api import users
from datetime import date, timedelta

from user_model import User
from entry_model import Entry
from entry_model import journal_key

import webapp2
import hashlib
import logging

months_dict = {
    "1": "January",
    "2": "February",
    "3": "March",
    "4": "April",
    "5": "May",
    "6": "June",
    "7": "July",
    "8": "August",
    "9": "September",
    "10": "October",
    "11": "November",
    "12": "December"
}

days_in_month = {
    "1": 31,
    "2": 28,
    "3": 31,
    "4": 30,
    "5": 31,
    "6": 30,
    "7": 31,
    "8": 31,
    "9": 30,
    "10": 31,
    "11": 30,
    "12": 31
}

weekdays = [
    "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"
]

def get_formatted_date_string(d):
    result = weekdays[d.weekday()] + ", " + months_dict[str(d.month)] + " " + str(d.day)
    return result

def get_past_entry(d, user):
    email_hash = hashlib.md5(user.email()).hexdigest()
    year_ago = d - timedelta(days=365)
    past_entry = Entry.query(ancestor=journal_key(email_hash)).filter(Entry.date == year_ago).fetch(1)
    if past_entry:
        message = past_entry[0].message
    else:
        month_ago = d - timedelta(days=days_in_month[str(d.month - 1)])
        past_entry = Entry.query(ancestor=journal_key(email_hash)).filter(Entry.date == d).fetch(1)
        if past_entry:
            message = past_entry[0].message
    if past_entry:
        return past_entry[0]

class DailyMailSender(webapp2.RequestHandler):
    def get(self):
        users = User.query().fetch(100)
        for user in users:
            self.send_message(user.user)

    def send_message(self, user):
        to_addr = user.email()
        message = mail.EmailMessage()
        # TODO: Use timezones to do this right. date.today() is one day ahead on Google's server's,
        # and I assume it's because they are on UTC.
        today = date.today() - timedelta(days=1)
        past_entry = get_past_entry(today, user)
        if past_entry:
            message_body = "On %s you wrote:\n\n%s" % (get_formatted_date_string(past_entry.date), past_entry.message)
        else:
            message_body = "How did your day go?"
        message.subject = "It's " + get_formatted_date_string(today) + " - How did your day go?"
        message.sender = "MyLife <entry+" + hashlib.md5(to_addr).hexdigest() + "_" + str(today.toordinal()) + "@oh-life.appspotmail.com>"
        message.to = to_addr
        message.body = message_body
        message.send()


app = webapp2.WSGIApplication([
    ('/mail/daily_email', DailyMailSender),
], debug=True)
