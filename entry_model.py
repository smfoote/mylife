from google.appengine.ext import ndb

def journal_key(journal_name):
    key_name = ndb.Key('Journal', journal_name)
    return key_name

class Entry(ndb.Model):
    message = ndb.TextProperty()
    user = ndb.UserProperty()
    date = ndb.DateProperty()
    images = ndb.BlobProperty(repeated=True)
