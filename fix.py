import os
import logging
import hashlib
import re
from datetime import date, datetime, timedelta
import time

from google.appengine.api import users
from google.appengine.ext import ndb

from user_model import User
from entry_model import Entry
from entry_model import journal_key

import jinja2
import webapp2

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class FixFormatting(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if user:
            email_hash = hashlib.md5(user.email()).hexdigest()
            start_date = self.get_start_date(self.request.get('startDate'))
            end_date = self.get_end_date(self.request.get('endDate'))
            date_range = False
            if start_date and end_date:
                date_range = {
                    'start_date': start_date,
                    'end_date': end_date
                }
            entries = self.get_past_entries(user, date_range)
            if entries:
                logging.info('Entries length: %d' % len(entries))
                for entry in entries:
                    message = entry.message
                    message_parts = re.split(r"On .*20[0-9]{2}.*\"?MyLife\"?.*\n", message)
                    logging.info('looking for carriage returns in %s' % message_parts[0])
                    matches = re.match(r".{0,15}[\n\r].{0,15}", message_parts[0])
                    logging.info('Carriage return: %s' % matches.group(0))
                    # entry.message = re.sub(r"([^\r])\r([^\r])", "\g<1> \g<2>", message_parts[0])
                    # logging.info('new entry: %s' % entry.message)
                ndb.put_multi(entries)

    def get_end_date(self, d):
        if not d:
            d = datetime.now()
        else:
            d = datetime.strptime(d, '%Y-%m-%d')
        return d

    def get_start_date(self, d):
        if not d:
            d = datetime.strptime('2000-01-01', '%Y-%m-%d')
        else:
            d = datetime.strptime(d, '%Y-%m-%d')
        return d

    def get_past_entries(self, user, date_range=False):
        email_hash = hashlib.md5(user.email()).hexdigest()
        end_date = date_range.get('end_date', date.today())
        start_date = date_range.get('start_date', (end_date - timedelta(days=20)))
        entries_query = Entry.query(Entry.date>=start_date, Entry.date<=end_date, ancestor=journal_key(email_hash)).order(Entry.date)
        entries = entries_query.fetch()
        return entries

app = webapp2.WSGIApplication([
    ('/fix', FixFormatting)
], debug=True)
