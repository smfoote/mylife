import webapp2
import logging
import hashlib
import re
from datetime import date

from google.appengine.ext import webapp
from google.appengine.api import mail
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext.webapp.mail_handlers import InboundMailHandler

from entry_model import Entry
from entry_model import journal_key

from google.appengine.ext import ndb
from google.appengine.api import users
from google.appengine.api import images

email_formatting_regex = r"([^\r])\r([^\r])"

class IncomingEntryHandler(InboundMailHandler):
    def get_entry_data_from_to_address(self, to_address):
        match = re.search(r"entry\+([a-z0-9]+)_([0-9]+)@", to_address)
        email_hash = match.group(1)
        entry_date = date.fromordinal(int(match.group(2)))
        return email_hash, entry_date

    def get_image_attachments(self, attachments):
        results = []
        if attachments:
            for attachment in attachments:
                filename, payload = attachment;
                file_ext = filename.split('.')[-1].lower()
                if file_ext == 'jpg' or file_ext == 'jpeg' or file_ext == 'gif' or file_ext == 'png':
                    img = images.Image(payload.decode())
                    if img.width > 1000:
                        img.resize(width=1000)
                    img.im_feeling_lucky()
                    img_output = img.execute_transforms(output_encoding=images.JPEG)
                    results.append(img_output)
        return results


    def receive(self, message):
        entry_message = ''
        user_email = re.search(r"<(.+)>", message.sender)
        user_email = user_email.group(1) if user_email else message.sender
        user_email_hash = hashlib.md5(user_email).hexdigest()
        to_hash, entry_date = self.get_entry_data_from_to_address(message.to)
        if user_email_hash == to_hash:
            existing_entry = Entry.query(ancestor=journal_key(user_email_hash)).filter(Entry.date == entry_date).fetch(1)
            plaintexts = message.bodies('text/plain')
            for content_type, body in plaintexts:
                entry_message = entry_message + body.decode()
            if hasattr(message, 'attachments'):
                images = self.get_image_attachments(message.attachments)
            entry_message_parts = re.split(r"On .*20[0-9]{2}.*\"?MyLife\"?.*\n", entry_message)
            entry_message = entry_message_parts[0]
            if existing_entry:
                # If an entry for the day already exists, append the new entry
                existing_entry = existing_entry[0]
                entry_message = existing_entry.message + "\n\n" + re.sub(email_formatting_regex, "\g<1> \g<2>", entry_message)
                existing_entry.message = entry_message
                if 'images' in locals():
                    existing_entry.images += images
                existing_entry.put()
            else:
                # create a brand new entry
                entry = Entry(parent=journal_key(user_email_hash))
                entry.user = users.User(message.sender)
                entry.date = entry_date
                entry.message = re.sub(email_formatting_regex, "\g<1> \g<2>", entry_message)
                if 'images' in locals():
                    entry.images = images
                entry.put()

app = webapp.WSGIApplication([
    IncomingEntryHandler.mapping()
], debug=True)

def main():
    run_wsgi_app(app)
if __name__ == "__main__":
    main()
