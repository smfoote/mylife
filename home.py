import os
import logging
import hashlib

from google.appengine.api import users

from user_model import User
from entry_model import Entry
from entry_model import journal_key

import jinja2
import webapp2

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class Home(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()

        if user:
            self.redirect("/past")
            signout_url = users.create_logout_url("/")
            self.response.write('If you would like to keep a journal using your email, <a href="%s">click here</a>' % signout_url)
        else:
            signin_url = users.create_login_url("/reg/create")
            self.response.write('If you would like to keep a journal using your email, <a href="%s">click here</a>' % signin_url)

app = webapp2.WSGIApplication([
    ('/', Home),
], debug=True)
