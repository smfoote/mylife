import os
import logging

from google.appengine.api import users
from google.appengine.ext import ndb

import webapp2

class Image(webapp2.RequestHandler):
    def get(self):
        entry_key = ndb.Key(urlsafe=self.request.get('img_id'))
        idx = int(self.request.get('idx'))
        entry = entry_key.get()
        if entry and entry.images:
            self.response.headers['Content-Type'] = 'image/jpg'
            self.response.out.write(entry.images[idx])

app = webapp2.WSGIApplication([
    ('/img', Image),
], debug=True)
