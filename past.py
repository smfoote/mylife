import os
import logging
import hashlib
from datetime import date, datetime, timedelta
import time

from google.appengine.api import users

from user_model import User
from entry_model import Entry
from entry_model import journal_key
# from entry_model import get_entry_by_date

import jinja2
import webapp2

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

def get_date_from_datestring(date_str):
    time_struct = time.strptime(date_str, '%Y-%m-%d')
    if time_struct:
        return datetime.fromtimestamp(time.mktime(time_struct))

class PastEntries(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()

        if not user:
            self.redirect("/")
        else:
            start_date = self.get_start_date(self.request.get('startDate'))
            end_date = self.get_end_date(self.request.get('endDate'))
            logging.info('START_DATE')
            logging.info(end_date)
            logging.info('START_DATE')
            date_range = False
            if start_date and end_date:
                date_range = {
                    'start_date': start_date,
                    'end_date': end_date
                }
            past_entries = self.get_past_entries(user, date_range)
            template = JINJA_ENVIRONMENT.get_template('tl/past.html')
            self.response.write(template.render(past_entries))

    def get_end_date(self, d):
        if not d:
            d = datetime.now()
        else:
            d = datetime.strptime(d, '%Y-%m-%d')
        return d

    def get_start_date(self, d):
        if not d:
            d = datetime.now() - timedelta(days=30)
        else:
            d = datetime.strptime(d, '%Y-%m-%d')
        return d

    def get_past_entries(self, user, date_range=False):
        email_hash = hashlib.md5(user.email()).hexdigest()
        end_date = date_range.get('end_date', date.today())
        start_date = date_range.get('start_date', (end_date - timedelta(days=20)))
        entries_query = Entry.query(Entry.date>=start_date, Entry.date<=end_date, ancestor=journal_key(email_hash)).order(Entry.date)
        entries = entries_query.fetch()
        return {
          'user': {
            'nickname': user.nickname(),
          },
          'entries': entries
        }

class PastEntry(webapp2.RequestHandler):
    def get(self, date_str):
        user = users.get_current_user()

        if not user:
            self.redirect("/")
        else:
            email_hash = hashlib.md5(user.email()).hexdigest()
            date = get_date_from_datestring(date_str)
            entry_query = Entry.query(Entry.date == date, ancestor=journal_key(email_hash))
            entry = entry_query.fetch()
            if entry:
                entry = entry[0]
            logging.info(entry.message)
            template = JINJA_ENVIRONMENT.get_template('tl/singleEntryView.html')
            self.response.write(template.render({'user': {'nickname': user.nickname()}, 'entry': entry}))

app = webapp2.WSGIApplication([
    ('/past', PastEntries),
    ('/past/entry/((?:20|19)\d{2}-\d{2}-[0-3]\d)', PastEntry)
], debug=True)
