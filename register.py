from google.appengine.api import users
from datetime import date

from user_model import User

import logging
import webapp2

class Register(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()

        if user:
            self.redirect("/reg/create")
        else:
            signin_url = users.create_login_url("/reg/create")
            self.response.write('If you would like to keep a journal using your email, <a href="%s">click here</a>' % signin_url)

class CreateUser(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()

        if user:
            all_users_query = User.query()
            user_count = all_users_query.count()
            logging.info("user email: " + user.email())
            existing_user = User.query(User.id == user.email()).fetch(1)
            if user_count < 100 and not existing_user:
                new_user = User(id=user.email(), user=user)
                new_user.put()
                self.redirect("/welcome")
            else:
                self.redirect("/")
        else:
            self.redirect("/reg/register")

app = webapp2.WSGIApplication([
    ('/reg/register', Register),
    ('/reg/create', CreateUser),
], debug=True)
