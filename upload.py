import os
import logging
import hashlib
import re
import time
from datetime import date, datetime

from google.appengine.api import users
from google.appengine.api import images

from user_model import User
from entry_model import Entry
from entry_model import journal_key

import jinja2
import webapp2

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)
def get_date_from_datestring(date_str):
    time_struct = time.strptime(date_str, '%Y-%m-%d')
    if time_struct:
        return datetime.fromtimestamp(time.mktime(time_struct))

date_regex = r'.*((?:19|20)[0-9]{2}-[0-9]{2}-[0-3][0-9]).*'

class Upload(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if user:
            template = JINJA_ENVIRONMENT.get_template('tl/upload.html')
            self.response.write(template.render({'user': {'nickname':user.nickname()}}))

    def post(self):
        user = users.get_current_user()
        if user:
            user_email = user.email()
            user_email_hash = hashlib.md5(user_email).hexdigest()
            file = self.request.POST['file']
            entries_text = file.value
            logging.info(entries_text)
            past_entries = re.split(date_regex, entries_text)
            logging.info(past_entries)
            date = None
            for idx, entry in enumerate(past_entries):
                if re.match(date_regex, entry):
                    date = get_date_from_datestring(entry)
                elif date:
                    new_entry = Entry(parent=journal_key(user_email_hash))
                    new_entry.user = user
                    new_entry.date = date
                    new_entry.message = entry
                    new_entry.put()
                    date = None

class Photo(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if user:
            template = JINJA_ENVIRONMENT.get_template('tl/uploadPhoto.html')
            self.response.write(template.render({'user': {'nickname':user.nickname()}}))

    def post(self):
        user = users.get_current_user()
        if user:
            date_str = self.request.get('date')
            image = self.request.get('image')
            filename = str(self.request.POST['image'].filename)
            if image and not date_str:
                date_match = re.match(date_regex, filename)
                if date_match:
                    date_str = date_match.group(1)
                else:
                    self.response.write('Date is required')
            date = get_date_from_datestring(date_str)
            entry = self.get_entry(date, user)
            image = images.Image(image)
            if image.width > 1000:
                image.resize(width=1000)
            image.im_feeling_lucky()
            img_output = image.execute_transforms(output_encoding=images.JPEG)
            entry.images += [img_output]
            key = entry.put()
            self.redirect('/past/entry/%s' % date_str)

    def get_entry(self, d, user):
        email_hash = hashlib.md5(user.email()).hexdigest()
        entry = Entry.query(ancestor=journal_key(email_hash)).filter(Entry.date == d).fetch(1)
        if entry:
            return entry[0]

app = webapp2.WSGIApplication([
    ('/upload', Upload),
    ('/upload/photo', Photo)
], debug=True)
