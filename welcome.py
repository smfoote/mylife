from google.appengine.api import users

import webapp2
import logging

class Welcome(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if not user:
            self.redirect("/reg/register")
        else:
            self.redirect("/")

app = webapp2.WSGIApplication([
    ('/welcome', Welcome),
], debug=True)
